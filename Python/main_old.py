import chomp
from os import path
from copy import deepcopy
import locale

locale.setlocale(locale.LC_ALL, "")

try:
    import pickle
except:
    import cpickle as pickle


def build_cache(run_length=100000, rows=3, cols=6):
    import progressbar

    widgets = [
        progressbar.Timer(),
        " ",
        progressbar.Bar(),
        " ",
        progressbar.Percentage(format="%(percentage)6.2f%%"),
        f" | (",
        progressbar.ETA(),
        ") ",
    ]
    bar = progressbar.ProgressBar(max_value=run_length, widgets=widgets)
    game = chomp.Chomp(rows=rows, columns=cols)
    player = "Human"
    win_ratio = {"Human": 0, "AI": 0}
    loss_streak = {"Human": 0, "AI": 0}
    print(f"Running {run_length:n} times...\n")
    for i in range(run_length):
        # if i < 1000 or i % 10000 == 0:
        bar.update(i)
        # print(f"\r{i}", end="")
        while not game.finished():
            # print(game)
            move = None
            if player != "AI":
                move = game.get_random_move()
            else:
                move = game.get_alphabeta_move(game.board())
            # print(f" {player} player chomps below/right {move}\n")
            game.do(move)
            player = {"AI": "Human", "Human": "AI"}[player]
        # print(game)
        # print(f"Done. {player} loses")
        # Invert player to get proper win and lose streaks
        win_ratio[[x for x in loss_streak if x != player][0]] += 1
        loss_streak[player] += 1
        loss_streak[[x for x in loss_streak if x != player][0]] = 0
        game.new_game()
        player = "Human"
    bar.finish()
    lfloat_size = len(f"{run_length:n}")
    print(f"\nFinished {run_length:n} runs\n")
    human_wins = win_ratio["Human"]
    ai_wins = win_ratio["AI"]
    human_ratio = (human_wins / run_length) * 100
    ai_ratio = (ai_wins / run_length) * 100
    hstr = f"{human_wins:n}".rjust(lfloat_size)
    astr = f"{ai_wins:n}".rjust(lfloat_size)
    print(f"Human wins: {hstr} ({human_ratio:11.5f}%)")
    print(f"   AI wins: {astr} ({ai_ratio:11.5f}%)")
    print()
    # print(loss_streak)


def aivai(run_length=1000, rows=3, cols=6):
    import progressbar

    widgets = [
        "[ ",
        progressbar.Timer(),
        " ] ",
        progressbar.Bar(),
        " (",
        progressbar.ETA(),
        ") ",
    ]
    bar = progressbar.ProgressBar(max_value=run_length, widgets=widgets)
    game = chomp.Chomp(rows=rows, columns=cols)
    player = "AI1"
    win_ratio = {"AI1": 0, "AI2": 0}
    loss_streak = {"AI1": 0, "AI2": 0}
    for i in range(run_length):
        if i < 1000 or i % 1000 == 0:
            bar.update(i)
        while not game.finished():
            move = game.get_alphabeta_move(game.board())
            game.do(move)
            player = {"AI1": "AI2", "AI2": "AI1"}[player]
        win_ratio[[x for x in loss_streak if x != player][0]] += 1
        loss_streak[player] += 1
        loss_streak[[x for x in loss_streak if x != player][0]] = 0
        game.new_game()

    print(f"\nFinished {run_length:n} runs")
    print(win_ratio)
    print(loss_streak)


if __name__ == "__main__":
    from sys import argv

    funcs = {"cache": build_cache, "bots": aivai}
    to_run = None
    rows = 3
    cols = 6
    run_length = 100_000
    args = argv[1:]
    if len(args) > 0:
        for arg in args:
            if arg.startswith("--cache"):
                to_run = funcs["cache"]
            elif arg.startswith("--length="):
                try:
                    run_length = int(arg.split("=")[1])
                except ValueError:
                    run_length = 100_000
            elif arg.startswith("--rows="):
                try:
                    rows = int(arg.split("=")[1])
                except ValueError:
                    rows = 3
            elif arg.startswith("--cols=") or arg.startswith("--columns="):
                try:
                    cols = int(arg.split("=")[1])
                except ValueError:
                    cols = 6
            elif arg.startswith("--bots"):
                to_run = funcs["bots"]
    if path.exists(path.join("cache", f"cache.pkl")):
        with open(path.join("cache", f"cache.pkl"), "rb") as f:
            tdict = pickle.load(f)
        chomp.memoized = deepcopy(tdict)
    # build_cache(run_length=10_000_000)
    to_run(rows=rows, cols=cols, run_length=run_length)
    with open(path.join("cache", f"cache.pkl"), "wb+") as f:
        pickle.dump(chomp.memoized, f)
