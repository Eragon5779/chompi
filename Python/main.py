import chomp
from os import path
import tkinter as tk
from PIL import Image, ImageTk
import serial
from serial.tools import list_ports
import sys
import re
from ast import literal_eval
import time

try:
    import pickle
except ImportError:
    import cpickle as pickle

from enum import Enum

scr_h, scr_w = 600, 800

check_ser = re.compile(r"\(\d{0,2},\d{0,2}\)")


def flip(val, max):
    if max - val < 0:
        return 0
    return max - val


class gamemodes(Enum):
    HVA = 0
    # HVH = 1
    # AVA = 2
    # TRAIN = 3


class Player(object):
    def __init__(self, type, name, movefunc):
        self.type = type
        self.name = name
        self.getmove = movefunc


class ChompGUI(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.attributes("-fullscreen", True)
        self.configure(background="black")
        self.rows = 4
        self.cols = 5
        self.title("ChomPi")
        self.scr_w = self.winfo_screenwidth()
        self.scr_h = self.winfo_screenheight()
        self.elems = []
        self.nmsq = self.load_image("normal_square.png")
        self.pssq = self.load_image("poison_square.png")
        self.black = self.load_image("black.png")
        self.add_ui_elems()
        print("Added UI Elements")
        self.after(0, self.play)

    def configure_serial(self):
        self.com_port = None
        for port in list_ports.comports():
            if "Arduino" in port[1]:
                self.com_port = port[0]
            elif "USB" in port[2]:
                self.com_port = port[0]
        if self.com_port is None:
            print("ERROR: Arduino not connected! Please check USB")
            sys.exit(1)
        self.ardy = serial.Serial(self.com_port, 9600)
        print("Loaded serial connection")
        print(self.ardy)

    def add_ui_elems(self):
        if len(self.elems) > 0:
            for elem in self.elems:
                elem.grid_forget()
            self.elems = []
        for row in range(self.rows):
            for col in range(self.cols):
                image = self.nmsq
                if row == self.rows - 1 and col == 0:
                    image = self.pssq
                self.elems.append(
                    tk.Label(self, image=image, background="black")
                )
                self.elems[-1].grid(row=row, column=col)

    def load_image(self, imagename):
        return ImageTk.PhotoImage(
            Image.open(path.join("images", imagename)).resize(
                (int(self.scr_h / 4), int(self.scr_h / 4))
            )
        )

    def get_human_move(self, board=None):
        if not board:
            self.ardy.readline()
            return
        possible = self.game.possible_moves(board)
        move = (-1, -1)
        while move not in possible:
            move = self.ardy.readline().decode("UTF8")
            if check_ser.match(move):
                move = literal_eval(move)
                print(move)
                move = move[::-1]
        # move = None
        # while True:
        #    move = input("Enter move: ")
        #    if check_ser.match(move):
        #        move = literal_eval(move)
        #        if move in possible:
        #            break
        #    print("Invalid move.")
        return move

    def remove_square(self, row, col):
        if row == 0 and col == 0:
            return
        columns, rows = self.grid_size()
        row = rows - row
        for label in self.grid_slaves():
            lrow = int(label.grid_info()["row"])
            lcol = int(label.grid_info()["column"])
            if lrow < row and lcol >= col:
                label.config(image=self.black)
                label.image = self.black

    def play(self, rows=4, cols=5, mode=gamemodes.HVA):
        print("INTO GAME LOOP")
        # self.add_ui_elems(rows, cols)
        # move[1] = col, move[0] = row
        self.game = chomp.Chomp(rows=rows, columns=cols)
        if mode == gamemodes.HVA:
            self.configure_serial()
            turn = 0  # Index of whose turn it is
            players = [
                Player("Human", "Human", self.get_human_move),
                Player("AI", "ChomPi", self.game.get_alphabeta_move),
            ]
            # 1,0 == 3,1
            while True:
                while not self.game.finished():
                    self.update()
                    print(self.game)
                    # GARBAGE = input("PRESS ENTER TO CONTINUE")
                    cur_player = players[turn % 2]
                    move = cur_player.getmove(self.game.board())
                    to_remove = move
                    # if cur_player.type == "Human":
                    #    move = (flip(move[0], rows - 1), move[0])
                    # elif cur_player.type == "AI":
                    # to_remove = (move[1], flip(move[0], rows - 1))
                    #    to_remove = (move[1], flip(move[0], rows - 1))
                    print(f"{players[turn % 2].name} chose {move}")
                    self.remove_square(row=to_remove[0], col=to_remove[1])
                    self.game.do(move)
                    self.update()
                    turn += 1
                    if players[turn % 2].name == "ChomPi":
                        time.sleep(1)
                if players[turn % 2].type == "AI":
                    move = players[turn % 2].getmove(self.game.board())
                    # to_remove = (move[1], flip(move[0], rows - 1))
                    self.remove_square(row=to_remove[0], col=to_remove[1])
                self.update()
                # cont = input("Play again? (y/n) ")
                # if cont.lower() in ["n", "no"]:
                #    break
                self.get_human_move()
                self.game.new_game()
                self.add_ui_elems()
                turn = 0


def load_cache(filepath=path.join("cache", "cache.pkl")):
    if path.exists(filepath):
        with open(filepath, "rb") as f:
            print("Loading cache...")
            chomp.memoized = pickle.load(f)
            print("Cache loaded")
    else:
        print("WARNING: Playing without cache. Gameplay will be slow")


if __name__ == "__main__":
    load_cache()
    gameui = ChompGUI()
    gameui.mainloop()
