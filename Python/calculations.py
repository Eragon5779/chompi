import chomp
from os import path
from copy import deepcopy
import locale
import json
import progressbar

try:
    import pickle
except:
    import cpickle as pickle

locale.setlocale(locale.LC_ALL, "")
MOVESET = {}

# {
# "3x6": Board size
#     {
#      "boards": [
#          Board before each move
#      ],
#      "moves": [
#          Move at each board (tuple)
#      ],
#      "win_ratio": {"AI": 0, "Human": 0},
#     }
# }


def run(run_length=100_000, rows=4, cols=5):
    global MOVESET
    widgets = [
        progressbar.Timer(),
        " ",
        progressbar.Bar(),
        " ",
        progressbar.Percentage(format="%(percentage)6.2f%%"),
        f" | (",
        progressbar.ETA(),
        ") ",
    ]
    bar = progressbar.ProgressBar(max_value=run_length, widgets=widgets)
    game = chomp.Chomp(rows=rows, columns=cols)
    if rows == 4 and cols == 5:
        print(game)
    player = "AI"
    win_ratio = {"Human": 0, "AI": 0}
    loss_streak = {"Human": 0, "AI": 0}
    MOVESET = {"boards": [], "moves": [], "win_ratio": {}}
    for i in range(run_length):
        bar.update(i)
        while not game.finished():
            move = None
            if player != "AI":
                move = game.get_random_move()
            else:
                board = game.board()
                # board = game.shrink_board(game.board())
                move = game.get_alphabeta_move(board)
            MOVESET["boards"].append(game.board())
            MOVESET["moves"].append(move)
            game.do(move)
            player = {"AI": "Human", "Human": "AI"}[player]
        win_ratio[[x for x in loss_streak if x != player][0]] += 1
        loss_streak[player] += 1
        loss_streak[[x for x in loss_streak if x != player][0]] = 0
        game.new_game()
        player = "AI"
    bar.finish()
    MOVESET["win_ratio"] = win_ratio


if __name__ == "__main__":
    if path.exists(path.join("cache", "cache.pkl")):
        with open(path.join("cache", "cache.pkl"), "rb") as f:
            chomp.memoized = pickle.load(f)
    run(rows=4, cols=5)
    if "_cache__alphabeta" in chomp.memoized:
        chomp.memoized.pop("_cache__alphabeta")
    with open(path.join("cache", f"cache.pkl"), "wb+") as f:
        pickle.dump(chomp.memoized, f)
    for col in range(2, 101):
        for row in range(2, col):
            print(f"\nRunning for size {col}x{row}...\n")
            run(rows=row, cols=col)
            print("\nFinished")
            if "_cache__alphabeta" in chomp.memoized:
                chomp.memoized.pop("_cache__alphabeta")
            with open(path.join("cache", f"cache.pkl"), "wb+") as f:
                pickle.dump(chomp.memoized, f)
            # with open(path.join("calcs", f"calc_{row}x{col}.json"), "w+") as f:
            #    json.dump(MOVESET, f)
