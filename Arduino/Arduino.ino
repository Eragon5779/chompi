#define WIDTH    5
#define HEIGHT   4
#define LED_LOOP 1
#define LED_WAVE 2

int rows[] = {47,49,51,53};
int cols[] = {2,3,4,5,6};
int button[] = {-1,-1};
int new_button[] = {-1,-1};

int led_rows[] = {45,47,49,51,53};
int led_cols[] = {46,48,50,52};

int ser_in;

void setup()
{
  Serial.begin(9600);
  for (int i = 0; i < HEIGHT; i++)
  {
    pinMode(rows[i], OUTPUT);
    //pinMode(led_rows[i], OUTPUT);
  }
  for (int i = 0; i < WIDTH; i++)
  {
    pinMode(cols[i], INPUT);
    //pinMode(led_cols[i], OUTPUT);
  }
}

// Writes a tuple out to serial
void write_out()
{
  button[0] = new_button[0];
  button[1] = new_button[1];
  Serial.println(
    "(" + String(button[0])
    + "," + String(button[1]) + ")"
  );
}

void loop()
{
  /// START SERIAL LOOP
  int avail = Serial.available();
  if (avail > 0)
  {
    ser_in = Serial.read();
  }
  /// END SERIAL LOOP

  /// START INPUT LOOP
  for (int row = 0; row < HEIGHT; row++)
  {
    digitalWrite(rows[row], HIGH);
    for (int col = 0; col < WIDTH; col++)
    {
      if (digitalRead(cols[col]) == HIGH)
      {
        new_button[0] = col;
        new_button[1] = row;
      }
    }
    digitalWrite(rows[row], LOW);
  }
  if (button[0] != new_button[0] ||
      button[1] != new_button[1])
  {
    write_out();
  }
  /// END INPUT LOOP

  /// START LED LOOP

  /// END LED LOOP
}
